CREATE TABLE sellers(  
    id int NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name VARCHAR(255) UNIQUE,
    rating int
);

CREATE TABLE items(  
    id int NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name VARCHAR(255),
    sellerId INTEGER REFERENCES sellers(id)
);

-- Insert data into sellers
INSERT INTO sellers (name, rating) VALUES
('Alice', '3'),
('Bob', '4'),
('John', '5'),
('Ayan', '5'),
('Lasse', '4'),
('Peter', '5'),
('Nilson', '4');


-- Insert data into items
INSERT INTO items (name, sellerid) VALUES
('Laptop', 1),
('Phone', 2),
('Tablet', 3),
('Book', 4),
('Headphone', 5),
('Microphone', 7),
('Cherger', 1),
('Connector', 2),
('Tablet', 3);


