# sellerdb Project

## Description
This project contains SQL scripts to set up a PostgreSQL database with two tables: `sellers` and `items`. It also includes sample data to populate these tables.

## Requirements
- Docker
- Git

## Setup Instructions

### 1. Install Docker
If you don't already have Docker installed, you can download and install it from the [official Docker website](https://www.docker.com/get-started).

If you're using a Mac, you can also install Docker using [Homebrew](https://brew.sh/). or you can use the following command to install Docker on Mac:
```
brew install --cask docker
```


### 2. Clone the Repository
Clone this repository to your local machine using Git.
```
HTTPS: git clone https://gitlab.com/katecher/sellerdb.git
or
ssh: git@gitlab.com:katecher/sellerdb.git

cd sellerbd
```

### 3. To start the PostgreSQL database, run the following commands:

```
docker build -t dbimage .
```
````
docker run --name sellerdb -p 5432:5432 dbimage
````

### 4. Connect to the PostgreSQL database using installed db client

### 5. Run the SQL scripts to select sellers and items data from the database

```
SELECT items.name AS Item, sellers.name AS Seller
FROM items
JOIN Sellers ON Items.sellerId = Sellers.id
WHERE Sellers.rating > 4
```